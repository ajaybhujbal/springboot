## This is a sample Java Springboot project.
#Multiple Components were required in this project .
#Install & Setting up Docker Desktop , Maven ,Java , Git , enable Kubernetes Cluster in Docker Desktop.


#Build spring boot jar using maven
mvn clean install -Dmaven.repo.local=/c/Users/abhuj/code/m2/repository -Dmaven.test.skip=true 

#Once Jar is Buil Successfully we will copy it our destination 
Copy-Item "E:\Chrome_Downloads\vsts-agent-win-x64-2.213.2\_work\1\s\target\demo-0.0.1-SNAPSHOT.jar" -Destination "E:\Springbootjar\"

#Build your Dockerfile
docker build -t spring:5.0 .

#Prepare your deployment.yaml & service.yaml file.

dp1.yaml #Single file for deployment & service

#Check part your existing pods & service 

kubectl get pods 
kubectl get svc 

# Deploy you application

kubectl create -f dp1.yaml 
#deployment & service will be done.

#Checkpart 
kubectl get pods 
kubectl get svc 
kubectl describe svc spring-boot-service

#Check logs 
kubectl logs podname