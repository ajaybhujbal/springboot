FROM openjdk:8-jre-alpine3.9

# copy the packaged jar file into our docker image
 COPY demo-0.0.1-SNAPSHOT.jar /demo.jar

EXPOSE 8080
# set the startup command to execute the jar
CMD ["java", "-jar", "/demo.jar"]
